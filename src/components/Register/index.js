
import { FastField, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import PasswordFieldCustom from '../../common/PasswordFieldCustom';
import TextFieldCustom from '../../common/TextFieldCustom';
import "../../style/register.css";
import SnackbarCpn from '../../common/SnackbarCpn';
import { Alert } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { Desktop, TabletMobile } from '../../common/responsive-helper';


const Register = () => {
    const [open, setOpen] = React.useState({
        err: false,
        success: false
    });
    const [msg, setMsg] = React.useState('')
    const initialValues = {
        email: '',
        password: '',
        check_password: ''
    }
    let navigate = useNavigate()
    const validationSchema = Yup.object().shape({
        email: Yup.string().required("Vui lòng nhập email"),
        password: Yup.string().required('No password provided.')
            .min(6, 'Mật khẩu quá ngắn tối thiểu 8 ký tự'),
        check_password: Yup.string().required("Vui lòng nhập lại mật khẩu")
            .oneOf([Yup.ref('password'), null], 'Mật khẩu chưa khớp')
    })
    const handleSubmit = (value) => {
        if (value.password !== value.check_password) {
        }
    }
    const handleClose = (event, reason) => {
        setOpen({
            err: false,
            success: false
        });
    };
    return (
        <>
            <Desktop>
                <div style={{ backgroundColor: '#f8f8f8', padding: '2.5rem 0px' }}>
                    <div className="container-register">
                        <div className="title-register">
                            Đăng ký tài khoản
            </div>
                        <div>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={value => handleSubmit(value)}
                            >
                                {
                                    formikProps => {
                                        const { values, errors, touched } = formikProps
                                        return (
                                            <Form>
                                                <FastField
                                                    className="mb-3"
                                                    name="email"
                                                    component={TextFieldCustom}
                                                    label="Tài khoản"
                                                    placeholder="Nhập email hoặc số điện thoại"
                                                    fullWidth
                                                />
                                                <FastField
                                                    className="mb-3 m-0"
                                                    name="password"
                                                    component={PasswordFieldCustom}
                                                    label="Password"
                                                    placeholder=""
                                                    fullWidth
                                                />
                                                <FastField
                                                    className="mb-3 m-0"
                                                    name="check_password"
                                                    component={PasswordFieldCustom}
                                                    label="Nhập lại mật khẩu"
                                                    placeholder=""
                                                    fullWidth
                                                />
                                                <button onClick={() => setOpen(true)} className="btn-register">ĐĂNG KÝ</button>
                                                <div className='hr-login-container'>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                    <span style={{ fontWeight: 200, margin: '0px 1rem' }}>Hoặc</span>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                </div>
                                                <div onClick={() => {
                                                    navigate('/sign-in')
                                                }} className="btn-sign-in">ĐĂNG NHẬP</div>
                                            </Form>
                                        )
                                    }
                                }
                            </Formik>
                            <div className="footer-box">
                                Hotline chăm sóc khách hàng: 0941111111
                    <br />
Copyright © Ziggi
                    </div>
                            <SnackbarCpn open={open.success} handleClose={handleClose} msg={msg} status={'success'} />
                            <SnackbarCpn open={open.err} handleClose={handleClose} msg={msg} status={'error'} />
                        </div>
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div style={{ backgroundColor: '#f8f8f8', padding: '2.5rem 0px' }}>
                    <div className="container-register-mobile">
                        <div className="title-register-mobile">
                            Đăng ký tài khoản
            </div>
                        <div>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={value => handleSubmit(value)}
                            >
                                {
                                    formikProps => {
                                        const { values, errors, touched } = formikProps
                                        return (
                                            <Form>
                                                <FastField
                                                    className="mb-3"
                                                    name="email"
                                                    component={TextFieldCustom}
                                                    label="Tài khoản"
                                                    placeholder="Nhập email hoặc số điện thoại"
                                                    fullWidth
                                                />
                                                <FastField
                                                    className="mb-3 m-0"
                                                    name="password"
                                                    component={PasswordFieldCustom}
                                                    label="Password"
                                                    placeholder=""
                                                    fullWidth
                                                />
                                                <FastField
                                                    className="mb-3 m-0"
                                                    name="check_password"
                                                    component={PasswordFieldCustom}
                                                    label="Nhập lại mật khẩu"
                                                    placeholder=""
                                                    fullWidth
                                                />
                                                <button onClick={() => setOpen(true)} className="btn-register">ĐĂNG KÝ</button>
                                                <div className='hr-login-container'>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                    <span style={{ fontWeight: 200, margin: '0px 1rem' }}>Hoặc</span>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                </div>
                                                <div onClick={() => {
                                                    navigate('/sign-in')
                                                }} className="btn-sign-in">ĐĂNG NHẬP</div>
                                            </Form>
                                        )
                                    }
                                }
                            </Formik>
                            <div className="footer-box">
                                Hotline chăm sóc khách hàng: 0941111111
                    <br />
Copyright © Ziggi
                    </div>
                            <SnackbarCpn open={open.success} handleClose={handleClose} msg={msg} status={'success'} />
                            <SnackbarCpn open={open.err} handleClose={handleClose} msg={msg} status={'error'} />
                        </div>
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default Register;