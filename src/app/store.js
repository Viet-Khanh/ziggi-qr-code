import { configureStore } from "@reduxjs/toolkit";
import user from '../redux-toolkit/slice'


const rootReducer = {
    user : user
}

const store = configureStore(
    {
        reducer : rootReducer
    }
)

export default store