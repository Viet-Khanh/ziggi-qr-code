import React, { useEffect } from 'react';
import '../../style/header.css'
import { Desktop, TabletMobile } from '../../common/responsive-helper';
import { useLocation, useNavigate } from 'react-router';
import HeaderMobile from "./HeaderMobile"
import CallIcon from '@mui/icons-material/Call';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import { useSelector, useDispatch } from 'react-redux';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Button, Dropdown, Menu } from 'antd';
import { requestLogout } from '../../redux-toolkit/slice';

const Header = () => {
    const [showContent, setShowContent] = React.useState(false)
    const location = useLocation()
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const attribute = location.pathname === "/"
    useEffect(() => {
        if (showContent) {
            document.body.style.overflow = 'hidden';
            document.documentElement.style.overflow = 'hidden';
        }
        else {
            document.body.style.overflow = 'unset';
            document.documentElement.style.overflow = 'unset';
        }
    }, [showContent])
    const user = useSelector(state => state.user)
    console.log('user' ,user);
    
    const menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: (
                        <div>
                            Thông tin tài khoản
                        </div>
                    ),
                },
                {
                    key: '2',
                    label: (
                        <div onClick={async() => {
                            const logout = await dispatch(requestLogout())
                            navigate('/sign-in')
                        }}>
                            Đăng xuất
                        </div>
                    ),
                }
            ]}
        />
    );

    return (
        <>
            <Desktop>
                <div className="header">
                    <div>
                        <a href='/'><img src="https://f52-zpg-r.zdn.vn/8558328955045040324/fb459a5ea9946cca3585.jpg" width="60px" /> </a>
                    </div>
                    <div className="header-item">
                        <a onClick={() => {
                            if (attribute) {
                                window.scroll(0, 60)
                            }
                            else {
                                navigate('/')
                            }
                        }} className="item">
                            Trang chủ
                </a>
                        <a onClick={() => {
                            if (attribute) {
                                window.scroll(0, 810)
                            }
                            else {
                                navigate('/')
                            }
                        }} className="item">
                            Sản phẩm
                </a>
                        <a onClick={() => {
                            if (attribute) {
                                window.scroll(0, 2150)
                            }
                            else {
                                navigate('/')
                            }
                        }} className="item">
                            Lợi ích
                </a>
                        <a onClick={() => {
                            if (attribute) {
                                window.scroll(0, 2560)
                            }
                            else {
                                navigate('/')
                            }
                        }} className="item">
                            Bảng giá
                </a>
                        <a className="item">
                            Tin tức
                </a>
                        <div className="separator"></div>
                        <div style={{ display: 'flex', cursor: 'pointer' }}>
                            {!localStorage.getItem('access_token') ? <div onClick={() => navigate('/sign-in')} style={{ marginRight: 5 }}>Đăng nhập</div>
                                : 
                                <Dropdown overlay={menu} placement="bottomLeft" arrow>
                                    <AccountCircleIcon />
                                </Dropdown>

                            }
                            <div style={{ marginLeft: 10 }}>Giỏ hàng</div>
                        </div>
                    </div>
                </div>
            </Desktop>

            <TabletMobile>
                <div className="header-mobile" style={{ position: 'relative' }}>
                    <HeaderMobile setShowContent={setShowContent} showContent={showContent} isLogin={user.isLogin}/>
                </div>
                {showContent &&
                    <div className="show-dropdown-content">
                        <div className="item-content">
                            <a href="/">Trang chủ</a>
                        </div>
                        <div className="item-content">
                            <a href="/">Sản phẩm</a>
                        </div>
                        <div className="item-content">
                            <a href="/">Lợi ích</a>
                        </div>
                        <div className="item-content">
                            <a href="/">Bảng giá</a>
                        </div>
                        <div className="item-content">
                            <a href="/">Tin tức</a>
                        </div>
                        <div className="title-helper">
                            Bạn cần hỗ trợ
                        </div>
                        <div className="contract">
                            <CallIcon />
                            <div style={{ marginLeft: 10 }}>Liên hệ: 0983216047</div>
                        </div>
                        <div className="contract">
                            <ContactMailIcon />
                            <div style={{ marginLeft: 10 }}>aaaaa@gmail.com</div>
                        </div>
                    </div>
                }
            </TabletMobile>
        </>
    );
};

export default Header;