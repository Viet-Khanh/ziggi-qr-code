import React from 'react';
import '../../style/user-manual.css'
import { Desktop, TabletMobile } from '../../common/responsive-helper';
const UserManual = () => {
    return (
        <>
            <Desktop>
                <div className='wrap-user-manual'>
                    <div style={{ width: '88%' }}>
                        <div className="title-user-manual">
                            Giới thiệu sản phẩm
<br />
của Card Visit thông minh
            </div >
                        <div className="container-item">
                            <div className='item'>
                                <img src="https://b-f54-zpg-r.zdn.vn/5772684745249001953/be0e7e374dfd88a3d1ec.jpg" width="400px" />
                                <div>Thiết kế nhiều mẫu mã</div>
                                <p>(.......................)</p>
                            </div>
                            <div className='item'>
                                <img src="https://f63-zpg-r.zdn.vn/8461347635189112415/099e0785344ff111a85e.jpg" width="400px" />
                                <div>Với nhiều màu sắc</div>
                                <p>(.......................)</p>
                            </div>
                            <div className='item'>
                                <img src="https://f51-zpg-r.zdn.vn/4963769374031022120/e8856a9f59559c0bc544.jpg" width="400px" />
                                <div>Và đặc biệt rất là sang trọng</div>
                                <p>(.......................)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div className='wrap-user-manual-mobile'>
                    <div>
                        <div className="title-user-manual-mobile">
                            Giới thiệu sản phẩm
của Card Visit thông minh
            </div >
                        <div className="container-item-mobile">
                            <div className='item'>
                                <img src="https://b-f54-zpg-r.zdn.vn/5772684745249001953/be0e7e374dfd88a3d1ec.jpg" width="400px" />
                                <div>Thiết kế nhiều mẫu mã</div>
                                <p>(.......................)</p>
                            </div>
                            <div className='item'>
                                <img src="https://f63-zpg-r.zdn.vn/8461347635189112415/099e0785344ff111a85e.jpg" width="400px" />
                                <div>Với nhiều màu sắc</div>
                                <p>(.......................)</p>
                            </div>
                            <div className='item'>
                                <img src="https://f51-zpg-r.zdn.vn/4963769374031022120/e8856a9f59559c0bc544.jpg" width="400px" />
                                <div>Và đặc biệt rất là sang trọng</div>
                                <p>(.......................)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default UserManual;