

export const stateConfig = {
    formAdd : [
        {
            name : 'name',
            label : 'Tên'
        },
        {
            name : 'workPhone',
            label : 'Số điện thoại',
        },
        {
            name : 'email',
            label : 'Email',
        },
        {
            label : 'Website',
            name : 'website',
        },
        {
            label : 'Tên địa chỉ',
            name : 'workAddressName',
        },
        {
            label : 'Link địa chỉ',
            name : 'workAddressLink',   
        },
        {
            label : 'Tên Zalo',
            name : 'zaloName',   
        },
        {
            label : 'Link Zalo',
            name : 'zaloLink',   
        },
        {
            label : 'Tên Facebook',
            name : 'facebookName',   
        },
        {
            label : 'Link Facebook',
            name : 'facebookLink',   
        },
        {
            label : 'Tên Youtube',
            name : 'youtubeName',   
        },
        {
            label : 'Link Youtube',
            name : 'youtubeLink',   
        },
        {
            label : 'Tên TikTok',
            name : 'tiktokName',   
        },
        {
            label : 'Link Tiktok',
            name : 'tiktokLink',   
        },
        {
            label : 'Tên ngân hàng',
            name : 'bankName',   
        },
        {
            label : 'Số ngân hàng',
            name : 'bankNumber',   
        },
    ]
}