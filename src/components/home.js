import React from 'react';
import Header from './Header';
import Footer from './Footer';
import BannerHome from './BannerHome';
import ConceptByHome from './ConceptByHome';
import UserManual from './UserManual';
import HomeBenefit from './HomeBenefit';
import PriceList from './PriceList';
import PromotionHome from './PromotionHome';
import { apiClient } from '../helper/request/api_client';

const Home = () => {
    const [file , setFile] = React.useState(null)
    const handleSubmit = async (e) => {
        e.preventDefault()
        let formData = new FormData()
        formData.append('file' , file)

        const data = await apiClient.post("/upload-file" , formData)

        console.log("data" ,data);
        
    }
    const handleFileChange = (e) => {
        setFile(e.target.files[0])
    }
    return (
        <div>
            <BannerHome />
            <ConceptByHome />
            <UserManual />
            <HomeBenefit />
            <PriceList />
            <PromotionHome />
            {/* <form
                onSubmit={handleSubmit}
             >
                <input type="file" name="file" onChange={handleFileChange} />
                <button type="submit">submit</button>
            </form>
            <img src="https://drive.google.com/uc?export=view&id=1ZwRNiSl9543hb1tY3Dm20WY5D8kEnxP4" /> */}
        </div>
    );
};

export default Home;