import Paragraph from "antd/lib/skeleton/Paragraph"
import { Alert, AlertTitle } from "@mui/material"
import { openNotificationWithIcon } from "../request/notification"

export const IconItem = {
    email: {
        icon: <i class="fa-solid fa-envelope" style={{ color: '#fcbc3b' }}></i>,
    },
    workPhone: {
        icon: <i class="fa-solid fa-phone" style={{ color: '#fcbc3b' }}></i>,
    },
    website: {
        icon: <i class="fa-solid fa-globe" style={{ color: '#fcbc3b' }}></i>,
    },
    workAddress: {
        icon: <i class="fa-solid fa-map" style={{ color: '#fcbc3b' }}></i>,
    },
    zalo: {
        icon: <spam style={{ color: '#fcbc3b', fontSize: '0.6em', marginLeft: -2 }}>Zalo</spam>,
    },
    facebook: {
        icon: <i class="fa-brands fa-facebook-f" style={{ color: '#fcbc3b' }}></i>,
    },
    youtube: {
        icon: <i class="fa-brands fa-youtube" style={{ color: '#fcbc3b' }}></i>,
    },
    tiktok: {
        icon: <i class="fa-brands fa-tiktok" style={{ color: '#fcbc3b' }}></i>,
    },
    bank: {
        icon: <i class="fa-solid fa-building-columns" style={{ color: '#fcbc3b' }}></i>,
    }
}

export const TitleItem = {
    email: "Email",
    workPhone: "Phone Number",
    website: "Website",
    workAddress: "Work Address",
    zalo: "Zalo",
    facebook: "Facebook",
    youtube: "Youtube",
    tiktok: "Tiktok",
    bank: "Bank"
}

export const HrefLink = {
    email: (item) => {
        return <a className="content-link" href={`mailto:${item}`} target="_blank">{item}</a>
    },
    workPhone: (item) => {
        return <a className="content-link" href={`tel:${item}`} target="_blank">{item}</a>
    },
    website: (item) => {
        return <a className="content-link" href={item} target="_blank">{item}</a>
    },
    workAddress: (item) => {
        return <a className="content-link" href={item.workAddressLink} target="_blank">{item.workAddressName}</a>
    },
    zalo: (item) => {
        return <a className="content-link" href={item.zaloLink} target="_blank">{item.zaloName}</a>
    },
    facebook: (item) => {
        return <a className="content-link" href={item.facebookLink} target="_blank">{item.facebookName}</a>
    },
    youtube: (item) => {
        return <a className="content-link" href={item.youtubeLink} target="_blank">{item.youtubeName}</a>
    },
    tiktok: (item) => {
        return <a className="content-link" href={item.tiktokLink} target="_blank">{item.tiktokName}</a>
    },
    bank: (item) => {
        return <div style={{ cursor: 'pointer' }} onClick={() => {
            navigator.clipboard.writeText(item.bankNumber)
            openNotificationWithIcon("success" ,'', "Copy thành công")
        }} className="content-link" >{`${item.bankName || ''}-${item.bankNumber || ''}`}</div>
    }
}
