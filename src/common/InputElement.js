import {
    Input,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    RadioGroup,
    Stepper,
    OutlinedInput
} from "@mui/material";
import { Autocomplete } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useEffect, useState } from "react";

export const COLORS = {
    LUXURY: "#FDBE1C",
    GOLD: "#FAD400",
    WHITE: "#FFF",
    TEXT: "#1A152E",
    INNER_BORDER: "#55545B",
    BACK_GROUND: "#F8F8F8",
    TEXT_GRAY: "#55545B",
    BACK_GROUND_GREY: "#F2F2F2",
    ICON_GRAY: "#C4C4C4",
    BLACK: "#000",
    LIGHT_PURPLE: "#ADA3D6",
    GREEN : '#326ebd'
}


export const SelectLabelField = styled(InputLabel)({
    "&.Mui-focused": {
      color: COLORS.LUXURY,
    },
    "&.MuiFormLabel-root": {
      fontSize: "1em",
      lineHeight: "1em",
    },
  });

export const SelectField = styled(Select)({
    "&.Mui-focused": {
        borderColor: COLORS.LUXURY,
        "& .MuiOutlinedInput-notchedOutline": {
            borderColor: COLORS.LUXURY,
        },
    },
    "& .MuiInputLabel-outlined": {
        lineHeight: "1em",
        fontSize: "1em",
    },
    "&.MuiOutlinedInput-root": {
        fontSize: "1em",
        height: "3.3em",
        "&:hover fieldset": {
            borderColor: COLORS.LUXURY,
        },
        "&:hover": {
            borderColor: COLORS.LUXURY,
        },
    },
});
export const StyledPasswordField = styled(OutlinedInput)({
    "& .MuiInputBase-colorPrimary:hover": {
        border: '1px solid COLORS.LUXURY',
    }
});
export const StyledTextField = styled(TextField)({
    "& label.Mui-focused": {
        color: COLORS.LUXURY,
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: COLORS.LUXURY,
    },
    "& .MuiInputLabel-outlined": {
        fontSize: "1em",
        "&.Mui-disabled": {
            "-webkit-text-fill-color": "rgba(0, 0, 0, 0.6)"
        },
    },
    "& .MuiOutlinedInput-root": {
        fontSize: "1em",
        "&:hover fieldset": {
            borderColor: COLORS.LUXURY,
        },
        "&.Mui-focused fieldset": {
            borderColor: COLORS.LUXURY,
        },
    },
    "& .MuiOutlinedInput-input": {
        height: "1em",
        lineHeight: "1em",
        "&.Mui-disabled": {
            "-webkit-text-fill-color": "rgba(0, 0, 0, 0.87)"
        },
    },
    "& .MuiInputLabel-formControl": {
        top: '-3px !important'
    }
});