import React, { useEffect } from 'react'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import Login from './components/Login'
import SignUp from './components/signup.component'
import Display from './components/display'
import Home from './components/home'
import Register from './components/Register'
import Header from './components/Header'
import Footer from './components/Footer'
import QrCode from './components/GenerateQrCodeAdmin'
import Contact from './components/ShowContactCustomer.js'
import LayoutCpn from './components/GenerateQrCodeAdmin/LeftMenu'

const Com = ({ children }) => (
  <div className="App">
    <Header />
    {children}
    <Footer />
  </div>
)

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route exact path="/" element={<Com><Home /></Com>} />
          <Route path="/sign-in" element={<Com><Login /></Com>} />
          <Route path="/sign-up" element={<Com><Register /></Com>} />
          <Route path="/user/:id" element={<Display />} />
          <Route path="/admin" element={<LayoutCpn />} />
        </Routes>
      </div>
    </Router>
  )
  {/* const Com = ({ children }) => (
  <div className="App">
    <Header />
    {children}
    <Footer />
  </div>
)
function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Com><Home /></Com>} />
        <Route path="/sign-in" element={<Com><Login /></Com>} />
        <Route path="/sign-up" element={<Com><Register /></Com>} />
        <Route path="/user" element={<Display />} />
      </Routes>
    </Router>
  ) */}
}
export default App