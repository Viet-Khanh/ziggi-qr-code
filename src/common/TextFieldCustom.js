import React from 'react';
import { TextField, FormHelperText } from '@mui/material';
import { StyledTextField } from './InputElement';

const TextFieldCustom = (props) => {
    const {
        field, form,
        type, label, placeholder, className, fullWidth, multiline, rows ,disabled=false ,variant="outlined"
    } = props

    const { name } = field

    const { errors, touched } = form
    
    const showError = errors[name] && touched[name]

    return (
            <StyledTextField
                disabled={disabled}
                type={type}
                fullWidth={fullWidth}
                className={className}
                multiline={multiline}
                rows={rows}
                id={name}
                label={label}
                placeholder={placeholder}
                {...field}
                variant={variant}
                error={showError}
                helperText={showError ? errors[name] : ''}
            />

    );
};

export default TextFieldCustom;