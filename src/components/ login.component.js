import React, { Component, useEffect, useState } from 'react'
import LoadingScreen from 'react-loading-screen';

export default function Login() {
    const [loading, setLoading] = useState(true)
    const handleSubmit = (e) => {

        console.log(e)
    }
    useEffect(() => {
        setTimeout(() => {
            setLoading(false)
        }, 1000)
    }, [])
    return (
        <div>
            {loading ? <LoadingScreen
                loading={true}
                bgColor="rgba(255,255,255,0.8)"
                spinnerColor="#9ee5f8"
                textColor="#676767"
                logoSrc=""
                text=""
            ></LoadingScreen> : <div className="auth-inner">
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <h3>Sign In</h3>
                        <div className="mb-3">
                            <label>Email address</label>
                            <input
                                type="email"
                                className="form-control"
                                placeholder="Enter email"
                            />
                        </div>
                        <div className="mb-3">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                placeholder="Enter password"
                            />
                        </div>
                        <div className="mb-3">
                            <div className="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="customCheck1"
                                />
                                <label className="custom-control-label" htmlFor="customCheck1">
                                    Remember me
            </label>
                            </div>
                        </div>
                        <div className="d-grid">
                            <button type="submit" className="btn btn-primary">
                                Submit
          </button>
                        </div>
                        <p className="forgot-password text-right">
                            Forgot <a href="#">password?</a>
                        </p>
                    </form>
                </div>}
        </div>
    )
}
