import { useMediaQuery } from 'react-responsive'


export const Desktop = ({ children }) => {
    const isDesktop = useMediaQuery({ minWidth: 992 })
    return isDesktop ? children : null
}

export const Mobile = ({ children }) => {
    const isMobile = useMediaQuery({ maxWidth: 767 })
    return isMobile ? children : null
}

export const TabletMobile = ({ children }) => {
    const isOnTabletMobile = useMediaQuery({ maxWidth: 991 })
    return isOnTabletMobile ? children : null
}
