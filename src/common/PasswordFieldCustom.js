import * as React from 'react';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Input from '@mui/material/Input';
import FilledInput from '@mui/material/FilledInput';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { SelectLabelField, StyledPasswordField } from './InputElement';

const PasswordFieldCustom = (props) => {
    const {
        field, form,
        type, label, placeholder, className, fullWidth, multiline, rows, disabled = false, variant = "outlined"
    } = props
    const [password, setPassWord] = React.useState('')
    const [passwordShow, setPassWordShow] = React.useState(false)
    const { name } = field

    const { errors, touched } = form
    const handleClickShowPassword = () => {
        setPassWordShow(!passwordShow)
    };

    const handleChange = (e) => {
        setPassWord(e.target.value)
    }

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    const showError = errors[name] && touched[name]
    return (
        <FormControl className={className} sx={{ m: 1, width: '100%' }} variant="outlined">
            <SelectLabelField htmlFor="outlined-adornment-password">Password</SelectLabelField>
            <StyledPasswordField
                id="outlined-adornment-password"
                type={passwordShow ? 'text' : 'password'}
                // value={password}
                // onChange={(e) => handleChange(e)}
                {...field}  
                error={showError}
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                        >
                            {passwordShow ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                    </InputAdornment>
                }
                label="Password"
            />
            {showError && <p className='error-text'>{errors[name]}</p>}
        </FormControl>
    );
};

export default PasswordFieldCustom;