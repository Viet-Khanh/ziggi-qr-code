import React, { useEffect } from 'react';
import QrCode from 'qrcode'

const QrCodeCustomer = ({url}) => {
    const [imgUrl, setImgUrl] = React.useState('')
    console.log("url" ,url);
    
    const generatorQrCode = (url) => {
        QrCode.toDataURL(url, function (err, urlImg) {
            console.log(urlImg)
            setImgUrl(urlImg)
        })
    }
    useEffect(() => {
        if(url){
            generatorQrCode(url)
        }
    } ,[url])
    return (
        <div className="app">
            <div style={{display : 'flex' , alignItems : 'flex-end'}}>
                {imgUrl && <img src={imgUrl} alt="" />}
                {imgUrl && <a style={{marginBottom : 15 , marginLeft : 10}} href={`${imgUrl}`} className="register-btn" download>Download</a>}
            </div>
        </div>
    );
};

export default QrCodeCustomer;