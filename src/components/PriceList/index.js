import React from 'react';
import { Desktop, TabletMobile } from '../../common/responsive-helper';

const PriceList = () => {
    const priceListArr = [
        {
            title: 'Thẻ Doanh Nghiệp',
            price: 'Liên hệ',
            img: 'https://f51-zpg-r.zdn.vn/4963769374031022120/e8856a9f59559c0bc544.jpg',
            content: '',
            sub: '',
            sale: ''
        },
        {
            title: 'Sticker VIP ID',
            price: '189,000₫',
            img: 'https://b-f54-zpg-r.zdn.vn/5772684745249001953/be0e7e374dfd88a3d1ec.jpg',
            content: 'Thiết kế hiện đại và năng động với thông tin cơ bản.',
            sub: '',
            sale: '270,000₫'
        },
        {
            title: 'Thẻ cá nhân Basic',
            price: '169,000₫',
            img: 'https://f63-zpg-r.zdn.vn/8461347635189112415/099e0785344ff111a85e.jpg',
            content: '',
            sub: '',
            sale: '260,000₫'
        },
        {
            title: 'Thẻ cá nhân Signature',
            price: '294,000₫',
            img: 'https://f52-zpg-r.zdn.vn/8558328955045040324/fb459a5ea9946cca3585.jpg',
            content: 'Thiết kế theo yêu cầu, mang bản sắc cá nhân rõ nét.',
            sub: '',
            sale: '382,000₫'
        }

    ]
    return (
        <>
            <Desktop>
                <div className="container-price-list">
                    <div style={{ width: '88%' }}>
                        <div className="title-price-list">
                            Bảng giá
                <br />
Card Visit thông minh
                </div>

                        <div className='box-price-list'>
                            {
                                priceListArr.map((item, idx) => {
                                    return (
                                        <div key={idx} className='box-price-list-item'>
                                            <div className="title">
                                                {item.title}
                                            </div>
                                            <div className="content">
                                                {item.content}
                                            </div>
                                            <div style={{ display: 'flex' }}>
                                                <div className="price">
                                                    {item.price}
                                                </div>
                                                <div className='sale'>
                                                    {item.sale}
                                                </div>
                                            </div>
                                            <div className="sub">
                                                Đăng ký
                                    </div>
                                            <div className='img'>
                                                <img src={item.img} width="300px" />
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div className="container-price-list-mobile">
                    <div>
                        <div className="title-price-list-mobile">
                            Bảng giá
Card Visit thông minh
                </div>

                        <div className='box-price-list-mobile'>
                            {
                                priceListArr.map((item, idx) => {
                                    return (
                                        <div key={idx} className='box-price-list-item-mobile'>
                                            <div className="title">
                                                {item.title}
                                            </div>
                                            <div className="content">
                                                {item.content}
                                            </div>
                                            <div style={{ display: 'flex' }}>
                                                <div className="price">
                                                    {item.price}
                                                </div>
                                                <div className='sale'>
                                                    {item.sale}
                                                </div>
                                            </div>
                                            <div className="sub">
                                                Đăng ký
                                    </div>
                                            <div className='img'>
                                                <img src={item.img} width="300px" />
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default PriceList;