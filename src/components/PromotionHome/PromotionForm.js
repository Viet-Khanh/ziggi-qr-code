import React from 'react';
import { FastField, Form, Formik } from 'formik';
import TextFieldCustom from '../../common/TextFieldCustom';
import * as Yup from 'yup';
import { apiClient } from '../../helper/request/api_client';
import moment from 'moment'
import { openNotificationWithIcon } from '../../request/notification';

const PromotionForm = () => {
    const initialValues = {
        name: '',
        email: '',
        phone: '',
        description: ''
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string().required("Vui lòng nhập tên"),
        email: Yup.string().required("Vui lòng nhập email"),
        phone: Yup.number().required("Vui lòng nhập số điện thoại")
    })

    const handleSubmit = async (value) => {
        console.log("value", moment().format("hh:mma DD-MM-YYYY"));
        try {
            const { data } = await apiClient.post("/contact" , {
                ...value,
                date : moment().format("hh:mma DD-MM-YYYY")
            })
            openNotificationWithIcon("success" , "Gửi thành công")
        } catch (error) {
            openNotificationWithIcon("error" , "Gửi thất bại")
        }
    }
    return (
        <div className="w-9">
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={value => handleSubmit(value)}
            >
                {
                    formikProps => {
                        const { values, errors, touched } = formikProps
                        return (
                            <Form>
                                <FastField
                                    className="mb-3"
                                    name="name"
                                    component={TextFieldCustom}
                                    label="Họ tên"
                                    placeholder="Tên của bạn"
                                    fullWidth
                                />
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <FastField
                                        className="mb-3 w-48"
                                        name="email"
                                        component={TextFieldCustom}
                                        label="Email"
                                        placeholder="Email của bạn"
                                        type="email"
                                    />
                                    <FastField
                                        className="mb-3 w-48"
                                        name="phone"
                                        component={TextFieldCustom}
                                        label="Số điện thoại"
                                        placeholder="Số điện thoại của bạn"
                                    />
                                </div>
                                <FastField
                                    className="mb-3"
                                    name="description"
                                    component={TextFieldCustom}
                                    label="Nội dung"
                                    placeholder=""
                                    fullWidth
                                    multiline
                                    rows={5}
                                />
                                <button className="btn">Gửi cho chúng tôi</button>
                            </Form>
                        )
                    }
                }
            </Formik>
        </div>
    );
};

export default PromotionForm;