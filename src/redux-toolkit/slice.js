import { createSlice } from '@reduxjs/toolkit'
import {
    finishedLoadingSuccess,
    finishedLoadingFailure,
    isLoadingRequest,
} from './slice_redux';
import { openNotificationWithIcon } from '../request/notification';
import { apiClient } from '../helper/request/api_client';

const initialState = {}

const slice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        loginRequest: (state, { payload }) => {
            console.log('loginRequest');
            
            isLoadingRequest(state);
            state.isLogin = false;
        },
        loginSuccess: (state, { payload }) => {
            console.log('loginSuccess');

            finishedLoadingSuccess(state);
            state.isLogin = true;
            state.role = payload.role;
        },
        loginFail: (state, { payload }) => {
            console.log('loginFail');

            finishedLoadingFailure(state)
            state.isLogin = false;
        },
        logout: (state) => {
            console.log('logout');

            finishedLoadingSuccess(state);
            state.isLogin = false;
        }
    }

});


export const requestLogin = (body) => async (dispatch) => {
    try {
        dispatch(loginRequest());

        const {data} = await apiClient.post("/login",body)
        console.log("data" ,data);
        
        await dispatch(loginSuccess(data.user));

        localStorage.setItem("access_token", data.user.token);
        localStorage.setItem("refresh_token", data.user.refresh_token);
        openNotificationWithIcon("success", "Đăng nhập thành công");
        return 1;
    } catch (err) {           
        openNotificationWithIcon("error", "Tài khoản hoặc mật khẩu không đúng ");
        dispatch(loginFail())
        return 0;
    }
}

export const requestLogout = (body) => async (dispatch) => {
    try {
        dispatch(logout())
        localStorage.clear();
        return 1;
    } catch (err) {
        dispatch(loginFail())
        return 0;
    }
}

// SELECTOR

// export const isLoginSelector = state => {
//     return state.app.isLogin
// };



const { actions, reducer } = slice;
export const { loginRequest, loginSuccess, loginFail, logout } = actions;
export default reducer;