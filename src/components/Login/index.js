
import { FastField, Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import PasswordFieldCustom from '../../common/PasswordFieldCustom';
import TextFieldCustom from '../../common/TextFieldCustom';
import "../../style/register.css";
import SnackbarCpn from '../../common/SnackbarCpn';
import { Alert, Checkbox } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { TabletMobile, Desktop } from "../../common/responsive-helper"
import { useDispatch } from 'react-redux';
import { requestLogin } from '../../redux-toolkit/slice';

const Login = () => {
    const [open, setOpen] = React.useState({
        err: false,
        success: false
    });
    const [msg, setMsg] = React.useState('')
    const initialValues = {
        email: JSON.parse(window.localStorage.getItem("account"))?.tk || '',
        password: JSON.parse(window.localStorage.getItem("account"))?.mk || '',
    }
    const [checked, setChecked] = React.useState(false)

    let navigate = useNavigate()
    const dispatch = useDispatch()
    const validationSchema = Yup.object().shape({
        email: Yup.string().required("Vui lòng nhập email"),
        password: Yup.string().required('No password provided.')
            .min(6, 'Mật khẩu quá ngắn tối thiểu 8 ký tự')
    })
    const handleSubmit = async (value) => {
        if (checked) {
            window.localStorage.setItem("account", JSON.stringify({
                "tk": value.email,
                "mk": value.password
            }))
        }
        const body = {email : value.email , password : value.password}
        const result = await dispatch(requestLogin(body))
        if(result == 1){
            navigate('/admin')
        }
    }
    const handleClose = (event, reason) => {
        setOpen({
            err: false,
            success: false
        });
    };
    return (
        <>
            <Desktop>
                <div style={{ backgroundColor: '#f8f8f8', padding: '2.5rem 0px' }}>
                    <div className="container-register">
                        <div className="title-register">
                            Đăng nhập
            </div>
                        <div>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={value => handleSubmit(value)}
                            >
                                {
                                    formikProps => {
                                        const { values, errors, touched } = formikProps
                                        return (
                                            <Form>
                                                <FastField
                                                    className="mb-3"
                                                    name="email"
                                                    component={TextFieldCustom}
                                                    label="Tài khoản"
                                                    placeholder="Nhập email hoặc số điện thoại"
                                                    fullWidth
                                                />
                                                <FastField
                                                    className="mb-3 m-0"
                                                    name="password"
                                                    component={PasswordFieldCustom}
                                                    label="Password"
                                                    placeholder=""
                                                    fullWidth
                                                />
                                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <Checkbox
                                                            checked={checked}
                                                            onChange={(e) => setChecked(e.target.checked)}
                                                            sx={{
                                                                color: "#fdbe1c",
                                                                '&.Mui-checked': {
                                                                    color: "#fdbe1c",
                                                                },
                                                            }}
                                                        />
                                                        <div>Lưu mật khẩu</div>
                                                    </div>
                                                    <div style={{ color: '#fdbe1c' }}>
                                                        Quên mật khẩu
                                            </div>
                                                </div>
                                                <button onClick={() => setOpen(true)} className="btn-register">ĐĂNG NHẬP</button>
                                                <div className='hr-login-container'>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                    <span style={{ fontWeight: 200, margin: '0px 1rem' }}>Hoặc</span>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                </div>
                                                <div onClick={() => {
                                                    navigate('/sign-up')
                                                }} className="btn-sign-in">ĐĂNG KÝ</div>
                                            </Form>
                                        )
                                    }
                                }
                            </Formik>
                            <div className="footer-box">
                                Hotline chăm sóc khách hàng: 0941111111
                    <br />
Copyright © Ziggi
                    </div>
                            <SnackbarCpn open={open.success} handleClose={handleClose} msg={msg} status={'success'} />
                            <SnackbarCpn open={open.err} handleClose={handleClose} msg={msg} status={'error'} />
                        </div>
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div style={{ backgroundColor: '#f8f8f8', padding: '2.5rem 0px' }}>
                    <div className="container-register-mobile">
                        <div className="title-register-mobile">
                            Đăng nhập
            </div>
                        <div>
                            <Formik
                                initialValues={initialValues}
                                validationSchema={validationSchema}
                                onSubmit={value => handleSubmit(value)}
                            >
                                {
                                    formikProps => {
                                        const { values, errors, touched } = formikProps
                                        return (
                                            <Form>
                                                <FastField
                                                    className="mb-3"
                                                    name="email"
                                                    component={TextFieldCustom}
                                                    label="Tài khoản"
                                                    placeholder="Nhập email hoặc số điện thoại"
                                                    fullWidth
                                                />
                                                <FastField
                                                    className="mb-3 m-0"
                                                    name="password"
                                                    component={PasswordFieldCustom}
                                                    label="Password"
                                                    placeholder=""
                                                    fullWidth
                                                />
                                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                                        <Checkbox
                                                            checked={checked}
                                                            onChange={(e) => setChecked(e.target.checked)}
                                                            sx={{
                                                                color: "#fdbe1c",
                                                                '&.Mui-checked': {
                                                                    color: "#fdbe1c",
                                                                },
                                                            }}
                                                        />
                                                        <div>Lưu mật khẩu</div>
                                                    </div>
                                                    <div style={{ color: '#fdbe1c' }}>
                                                        Quên mật khẩu
                                            </div>
                                                </div>
                                                <button onClick={() => setOpen(true)} className="btn-register">ĐĂNG NHẬP</button>
                                                <div className='hr-login-container'>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                    <span style={{ fontWeight: 200, margin: '0px 1rem' }}>Hoặc</span>
                                                    <div style={{ flex: 1, height: 1, background: '#e2e2e2' }}></div>
                                                </div>
                                                <div onClick={() => {
                                                    navigate('/sign-up')
                                                }} className="btn-sign-in">ĐĂNG KÝ</div>
                                            </Form>
                                        )
                                    }
                                }
                            </Formik>
                            <div className="footer-box">
                                Hotline chăm sóc khách hàng: 0941111111
                    <br />
Copyright © Ziggi
                    </div>
                            <SnackbarCpn open={open.success} handleClose={handleClose} msg={msg} status={'success'} />
                            <SnackbarCpn open={open.err} handleClose={handleClose} msg={msg} status={'error'} />
                        </div>
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default Login;