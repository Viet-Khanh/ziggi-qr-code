import React from 'react';
import "../../style/common.css"
import { Desktop, TabletMobile } from '../../common/responsive-helper';
const HomeBenefit = () => {
    const benefitArr = [
        {
            title: 'Liên kết nhanh tới danh bạ và các mạng xã hội',
            content: 'Người nhận có thể gọi điện, lưu số và theo dõi bạn ngay trên các mạng xã hội như Facebook, Zalo, Instagram, Youtube...'
        },
        {
            title: 'Thay đổi thông tin mà không cần in lại card',
            content: 'Bạn có thể tùy chỉnh avatar và các thông tin khác một cách dễ dàng mà không cần làm lại Card Visit'
        },
        {
            title: 'Xây dựng thương hiệu cá nhân chuyên nghiệp',
            content: 'Chiếc Card này giúp bạn thể hiện sự chuyên nghiệp của mình trước đối tác, khách hàng, hỗ trợ công việc thuận lợi hơn.'
        }
    ]
    return (
        <>
            <Desktop>
                <div className="container-benefit">
                    <div style={{ width: '88%' }}>
                        <div className="title-benefit">
                            Những ưu điểm của
<br />
Card Visit thông minh

                </div>
                        <div className="box">
                            {benefitArr.map((item, idx) => {
                                return (
                                    <div key={idx} className='box-item'>
                                        <div className='title-box'>
                                            {item.title}
                                        </div>
                                        <div className="separator-box"></div>
                                        <p>
                                            {item.content}
                                        </p>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div className="container-benefit-mobile">
                        <div className="title-benefit-mobile">
                            Những ưu điểm của
Card Visit thông minh
                </div>
                        <div className="box-mobile">
                            {benefitArr.map((item, idx) => {
                                return (
                                    <div key={idx} className='box-item-mobile'>
                                        <div className='title-box'>
                                            {item.title}
                                        </div>
                                        <div className="separator-box"></div>
                                        <p>
                                            {item.content}
                                        </p>
                                    </div>
                                )
                            })}
                        </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default HomeBenefit;