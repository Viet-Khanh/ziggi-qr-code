import React from 'react';
import '../../style/banner-home.css'
import { Desktop, TabletMobile } from '../../common/responsive-helper';
const BannerHome = () => {
    return (
        <>
            <Desktop>
                <div style={{ display: 'flex', justifyContent: 'center'}}>
                    <div className="banner-home">
                        <div className="register">
                            <div className="title-home">
                                Để lại dấu ấn cá nhân
                                chỉ với một chạm
                </div>
                            <p className='leading-relaxed w-8'>
                                Quét mã nhanh để trao danh thiếp tới đồng
                                nghiệp, đối tác, khách hàng.
                </p>
                            <a href="/sign-up" className="register-btn">Đăng ký</a>
                        </div>
                        <div style={{ flex: 1, display: 'flex', justifyContent: "flex-end" }}>
                            <img src='https://b-f52-zpg-r.zdn.vn/4532166695733687179/c3e8ba0e89c44c9a15d5.jpg' width="500px" />
                        </div>
                        <hr />
                    </div>
                </div>
            </Desktop>

            <TabletMobile>
                <div>
                    <div className="banner-home-mobile">
                        <div className="register">
                            <div className="title-home-mobile">
                                Để lại dấu ấn cá nhân
                                chỉ với một chạm
                </div>
                            <p className='leading-relaxed-mobile'>
                                Quét mã nhanh để trao danh thiếp tới đồng
                                nghiệp, đối tác, khách hàng.
                </p>
                            <a href="/sign-up" className="register-btn">Đăng ký</a>
                        </div>
                        <div style={{ flex: 1, display: 'flex', justifyContent: "flex-end" }}>
                            <img src='https://b-f52-zpg-r.zdn.vn/4532166695733687179/c3e8ba0e89c44c9a15d5.jpg' width="100%" />
                        </div>
                        <hr />
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default BannerHome;