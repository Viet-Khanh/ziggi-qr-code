import React from 'react';
import "../../style/footer.css"
import { Desktop, TabletMobile } from '../../common/responsive-helper';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import FavoriteIcon from '@mui/icons-material/Favorite';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import AddIcCallIcon from '@mui/icons-material/AddIcCall';
import EmailIcon from '@mui/icons-material/Email';


const Footer = () => {
    const [value, setValue] = React.useState(0);
    return (
        <>
            <Desktop>
                <div className="wrap-footer">
                    <div className="footer">
                        <div>
                            <img src="https://f52-zpg-r.zdn.vn/8558328955045040324/fb459a5ea9946cca3585.jpg" width="60px" />
                        </div>
                        <div className="footer-block">
                            <div>
                                <div>CTCP CÔNG NGHỆ BẤT ĐỘNG SẢN RESTA</div>
                                <p>15 Hoàng Quốc Việt, P.Phú Thuận, Quận 7, TP Hồ Chí Minh</p>
                                <p>0943 999 999</p>
                                <p>dkhanh292000</p>
                            </div>
                            <div>
                                <div>Liên kết</div>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                            </div>
                            <div>
                                <div>Liên kết</div>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                            </div>
                            <div>
                                <div>Liên kết</div>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                                <p>Chính sách đổi trả</p>
                            </div>
                        </div>
                    </div>
                    <div>
                    Copyright © 2022 VIPID. Powered by GCO Group
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <Box style={{ position: 'fixed', left: 0, bottom: 0, width: '100vw' }}>
                    <BottomNavigation
                        showLabels
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        style={{justifyContent : 'space-around'}}
                    >
                        <BottomNavigationAction icon={<a href="tel:0983216047" style={{color : '#000'}}><AddIcCallIcon /></a>} />
                        <BottomNavigationAction icon={<a href="//zalo.me/0983216047" target="_blank"><img src="https://phukiencongnghe365.com/wp-content/uploads/2021/06/zalo-icon-400x400-1.png" width="50%"/></a>} />
                        <BottomNavigationAction icon={<a href='//m.me/100006364240644' target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Facebook_Messenger_logo_2020.svg/2048px-Facebook_Messenger_logo_2020.svg.png" width="80%" style={{marginBottom : '1.2rem'}}/></a>}/>
                        <BottomNavigationAction icon={<a href="mailto:dkhanh292000@gmail.com" style={{color : '#000'}}><EmailIcon /></a>} />
                        <BottomNavigationAction icon={<a style={{color : '#000'}}><LocationOnIcon /></a>} />
                    </BottomNavigation>
                </Box>
            </TabletMobile>
        </>
    );
};

export default Footer;