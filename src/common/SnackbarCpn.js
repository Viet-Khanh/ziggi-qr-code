import React from 'react';
import Alert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

const SnackbarCpn = (props) => {
    const {open , handleClose , status='success' , msg} = props
    return (
        <Snackbar open={open} anchorOrigin={{ vertical: "top", horizontal: 'right' }} onClose={handleClose}>
            <Alert onClose={handleClose} severity={status} sx={{ width: '100%' }}>
                {msg}
            </Alert>
        </Snackbar>
    );
};

export default SnackbarCpn;