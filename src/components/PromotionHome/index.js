import React from 'react';
import PromotionForm from './PromotionForm';
import { TabletMobile, Desktop } from '../../common/responsive-helper';

const PromotionHome = () => {
    return (
        <>
            <Desktop>
                <div className="container-promotion">
                    <div className="promotion-element">
                        <div style={{ flex: 1 }}>
                            <div className='title-promotion'>Gửi liên hệ cho chúng tôi !!!</div>
                            {/* <p className="content-promotion">Giá ưu đãi chỉ áp dụng trong tháng 5/2021. Đừng bỏ lỡ!</p> */}
                            <PromotionForm />
                        </div>
                        <div style={{ flex: 1 }}>
                            <img src="https://theme.hstatic.net/200000329903/1000802639/14/9.jpg?v=34" width="100%" />
                        </div>
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div className="container-promotion-mobile">
                    <div className="promotion-element-mobile">
                        <div style={{ flex: 1 , marginBottom : '1rem'}}>
                            <div className='title-promotion-mobile'>Gửi liên hệ cho chúng tôi !!!</div>
                            {/* <p className="content-promotion">Giá ưu đãi chỉ áp dụng trong tháng 5/2021. Đừng bỏ lỡ!</p> */}
                            <PromotionForm />
                        </div>
                        <div style={{ flex: 1 }}>
                            <img src="https://theme.hstatic.net/200000329903/1000802639/14/9.jpg?v=34" width="100%" />
                        </div>
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default PromotionHome;