import React from 'react';
import { apiClient } from '../helper/request/api_client';
import { useLocation, useNavigate, useParams } from 'react-router';
import { IconItem, TitleItem, HrefLink } from '../helper/display-helper'
import axios from 'axios';
import { Desktop, TabletMobile } from '../common/responsive-helper';
import { Button } from 'antd';

const Display = () => {
    const [data, setData] = React.useState({})
    const [imageAvatar , setImageAvatar] = React.useState(null)
    const params = useParams()
    const navigate = useNavigate()
    const _requestData = async () => {
        try {
            const { data } = await apiClient.get(`/customer/${params.id}`)
            setData(data)

        } catch (error) {
            navigate('/')
        }
    }

    React.useEffect(() => {
        _requestData()
    }, [])

    return (
        <div className="container-display" style={{ display: 'flex', justifyContent: 'center' }}>
            <Desktop>
                <div className="wrap-display">
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: 40 }}>
                        <img className="avatar" src={`https://drive.google.com/uc?export=view&id=${data?.imageAvatar}`} />
                        <div style={{ fontSize: 30, fontWeight: 700 }}>{data?.name}</div>
                        <p>Connecting Life</p>
                    </div>
                    <div>
                        {data && Object.keys(data).filter(i => IconItem[i] && (typeof data[i] == "object" ? data[i][Object.keys(data[i])[0]] : data[i])).map((item, idx) => {     
                            return (
                                <div key={idx} style={{
                                    display: 'flex', alignItems: 'center', borderTop: idx ? 'none' : '1px solid #424340', borderBottom: '1px solid #424340', paddingRight: '10px',
                                    position: 'relative',
                                    left: 1000,
                                    animation: "myfirst 2s forwards",
                                    animationDelay: `${idx * 0.5}s`,
                                }} >
                                    <div style={{ padding: '0px 10px' }}>
                                        {IconItem[item].icon}
                                    </div>
                                    <div style={{ padding: "10px 0px" }}>
                                        <p className="title">{TitleItem[item]}</p>

                                        {HrefLink[item](data[item])}
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </Desktop>
            <TabletMobile>
                <div className="wrap-display" style={{ width: "95%" }}>
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: 40 }}>
                        <img className="avatar" src={`https://drive.google.com/uc?export=view&id=${data?.imageAvatar}`} />
                        <div style={{ fontSize: 30, fontWeight: 700 }}>{data?.name}</div>
                        <p>Connecting Life</p>
                    </div>
                    <div>
                        {Object.keys(data).filter(i => IconItem[i] && (typeof data[i] == "object" ? data[i][Object.keys(data[i])[0]] : data[i])).map((item, idx) => {
                            // if (IconItem[item]) {
                            return (
                                <div key={idx} style={{
                                    display: 'flex', alignItems: 'center', borderTop: idx ? 'none' : '1px solid #424340', borderBottom: '1px solid #424340', paddingRight: '10px',
                                    position: 'relative',
                                    left: 1000,
                                    animation: "myfirst 2s forwards",
                                    animationDelay: `${idx * 0.5}s`,
                                }} >
                                    <div style={{ padding: '0px 10px' }}>
                                        {IconItem[item].icon}
                                    </div>
                                    <div style={{ padding: "10px 0px" }}>
                                        <p className="title">{TitleItem[item]}</p>

                                        {HrefLink[item](data[item])}
                                    </div>
                                </div>
                            )
                            // }
                        })}
                    </div>
                </div>
            </TabletMobile>
            <Button style={{
                position: 'fixed', right: 35, bottom: 35, width: 60, height: 60, background: 'rgb(188, 134, 6)',
                borderRadius: '50%'
            }}>
                <a href={`http://20.115.75.139:5002/vcf/${data._id}`}>
                    <i class="fa-solid fa-user-plus" style={{ color: '#fff' }}></i>
                </a>
            </Button>
            <div class="bird-container bird-container--one">
                <div class="bird bird--one"></div>
            </div>

            <div class="bird-container bird-container--two">
                <div class="bird bird--two"></div>
            </div>

            <div class="bird-container bird-container--three">
                <div class="bird bird--three"></div>
            </div>

            <div class="bird-container bird-container--four">
                <div class="bird bird--four"></div>
            </div>
        </div >
    );
};

export default Display;