import React from 'react';
import "../../style/concept-by-home.css"
import { Desktop, TabletMobile } from '../../common/responsive-helper';

const ConceptByHome = () => {
    return (
        <>
            <Desktop>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <div className='concept-by-home'>
                        <div className="concept-img">
                            <img src="https://b-f52-zpg-r.zdn.vn/7996424893992522074/0ed82b3b18f1ddaf84e0.jpg" width="500px" />
                        </div>
                        <div style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                            <div className="concept-title">
                                Card Visit số
                    <br />
                    trong thời đại 4.0
                </div>
                            <div className="concept-content">
                                Tạm biệt những chiếc Card Visit giấy thông thường, đã đến lúc thông tin cá nhân của bạn được số hóa để chuyển tới mọi người một cách nhanh chóng và hiện đại hơn.
                </div>
                        </div>
                    </div>
                </div>
            </Desktop>

            <TabletMobile>
                <div>
                    <div className='concept-by-home-mobile'>
                        <div style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                            <div className="concept-title-mobile">
                                Card Visit số
                    <br />
                    trong thời đại 4.0
                </div>
                            <div className="concept-content">
                                Tạm biệt những chiếc Card Visit giấy thông thường, đã đến lúc thông tin cá nhân của bạn được số hóa để chuyển tới mọi người một cách nhanh chóng và hiện đại hơn.
                </div>
                        </div>
                        <div className="concept-img">
                            <img src="https://b-f52-zpg-r.zdn.vn/7996424893992522074/0ed82b3b18f1ddaf84e0.jpg" width="100%" />
                        </div>
                    </div>
                </div>
            </TabletMobile>
        </>
    );
};

export default ConceptByHome;