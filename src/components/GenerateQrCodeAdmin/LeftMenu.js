import { ContactsOutlined, UserOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import AddQrCode from '../GenerateQrCodeAdmin';
import Contact from '../ShowContactCustomer.js';
const { Header, Sider, Content } = Layout;

const LayoutCpn = (props) => {
    const [collapsed, setCollapsed] = useState(false);
    const [selectedKeys , setSelectedKeys] = useState("1")
    const navigate = useNavigate()

    const handleClick =(e) => {
        setSelectedKeys(e.key)
    }
    return (
        <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="logo" />
                <Menu
                    theme="dark"
                    mode="inline"
                    selectedKeys={selectedKeys}
                    onClick={handleClick}
                    items={[
                        {
                            key: '1',
                            icon: <UserOutlined />,
                            label: 'Quản lý User',
                        },
                        {
                            key: '2',
                            icon: <ContactsOutlined />,
                            label: 'Contact',
                        }
                    ]}
                />
            </Sider>
            <Layout className="site-layout">
                <Content
                    className="site-layout-background"
                    style={{
                        // margin: '24px 16px',
                        padding: 14,
                        minHeight: "90vh"
                    }}
                >
                    {selectedKeys == 1 ? <AddQrCode /> : <Contact /> }
                </Content>
            </Layout>
        </Layout>
    );
};

export default LayoutCpn;